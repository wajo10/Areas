from math import pi
def area_esfera(Radio):
    """	Tecnológico de Costa Rica
	Ing. Computadores
	Maria José Zamora Vargas 2018026607 y Wajib Zaglul 2018099304
	Calculador de áreas
	entradas: Radio
	restricciones: los valores de radio deben ser enteros mayores a 0"""
    Area=4*pi*Radio**2
    return Area

def area_piramide(Ancho,Largo,Altura):
    """	Tecnológico de Costa Rica
	Ing. Computadores
	Maria José Zamora Vargas 2018026607 y Wajib Zaglul 2018099304
	Calculador de áreas
	entradas: Ancho, Largo y Apotema de la pirámide
	restricciones: los valores deben ser enteros mayores a 0"""
    Area= (Ancho*Largo)+((Ancho*2+Largo*2)*Altura)/2
    return Area
